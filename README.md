# Python XML Formatter

An script that format/pretty print an XML, also decode html entities

## Reference

- [Formatear XML Usando Python O PHP](https://www.librebyte.net/programacion/formatear-xml-usando-python-o-php/)
- [Formatting XML Using Python Or PHP](https://www.librebyte.net/en/programming/formatting-xml-using-python-or-php/)
- [Video ES](https://youtu.be/mKjXmGLz8Ck)