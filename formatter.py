#!/usr/bin/env python3

from argparse import ArgumentParser
from pathlib import Path
from xml.dom.minidom import parseString
import html

parser = ArgumentParser(description="XML formatter")

parser.add_argument('-f', '--file', required=True, dest="file", type=Path, help="Source file to format")
args = parser.parse_args()

file = args.file

if not file.is_file():
    print(f"File {file} does not exist or is not readable!")
    exit(1)

content = file.read_text()
if not content:
    print(f"File {file} is empty, nothing to do!")
    exit(2)

xml = parseString(html.unescape(content))
print(xml.toprettyxml())
exit(0)


